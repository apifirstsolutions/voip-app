# Emergency-App
## **Production Setup**
### Deploy Package
- Obtain `package.tar.gz` and extract to `/var/www/html/Emergency` 
- Point webserver's public web directory to `/var/www/html/Emergency/src/public`
### Set Environment Variables for Emergency-App
- Copy `Emergency/src/.env.example` to new file `Emergency/src/.env`
- Set the following required environment variables

Variable | Required | Description | Example
--- | --- | --- | ---
APP_NAME | Yes | Application Name | Emergency
APP_ENV | Yes | local or production | local
APP_URL | Yes | Home URL. Also used as Push Server URL | http://127.0.0.1
APP_TIMEZONE | Yes | TZ used by app | UTC
DB_CONNECTION | Yes | DB Driver used. Use `mysql` for mariadb | mysql
DB_HOST | Yes | Database Host | 127.0.0.1
DB_PORT | Yes | Database Port | 3306
DB_DATABASE | Yes | Database Name | appdb
DB_USERNAME | Yes | DB User | user
DB_PASSWORD | Yes | DB User password | userpass
AUDIO_STREAM_SCRIPT | Yes | Location of `stream.sh` | /var/www/html/Emergency/stream.sh
AUDIO_STREAM_PORT | Yes | Audion Stream RTP port | 5000
AUDIO_FILES_LOCATION | Yes | Directory of audio files | /var/www/html/Emergency/src/public/audio


### Run database migration scripts

```
$ /var/www/html/Emergency/artisan migrate
```
### Setup cron to run Lumen scheduler every minute
- Add this in cron
```
$ crontab -e

* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1
```
- Restart cron
```
$ /etc/init.d/cron reload
```
### Setup GStream for RTP audio stream 
- Install GStream if not yet installed
```
$ sudo apt-get install gstreamer1.0-tools
```
- Check if audio streamer working
```
$ gst-launch-1.0 -m filesrc location=/var/www/html/Emergency/src/public/audio/audio1.wav ! wavparse ! audioconvert ! audioresample ! alawenc ! rtppcmapay ! udpsink host=127.0.0.1 port=5000
```


That's it! Access application at `<APP_URL>/admin`



==========
## **Development Environment Setup**

This will create a dockerized stack for a Lumen application, consisted of the following containers:

-  **app**, your PHP application container

        Nginx, PHP7.4 PHP7.4-fpm, Composer, NPM, Node.js v10.x
    
-  **mariadb**, [Bitnami MariaDB](https://github.com/bitnami/bitnami-docker-mariadb)

#### **Directory Structure**
```
+-- src <project root>
+-- resources
|   +-- default
|   +-- nginx.conf
|   +-- supervisord.conf
|   +-- www.conf
+-- .gitignore
+-- Dockerfile
+-- docker-compose.yml
+-- README.md <this file>
```

#### **Setup instructions**

**Prerequisites:** 

* Depending on your OS, the appropriate version of Docker Community Edition has to be installed on your machine.  ([Download Docker Community Edition](https://hub.docker.com/search/?type=edition&offering=community))

**Installation steps:** 
1. Clone this repository.
2. Copy `./src/.env-example` as `./src/.env`.
```
$ cd voip-app
$ cp ./src/.env-example ./src/.env
```
3. Start containers:
```
$ docker-compose up -d
```
This will download/build all the required images and start the stack containers. It usually takes a bit of time, so grab a cup of coffee.
4. After the whole stack is up, verify that the containers are running:
```
$ docker ps
```
5. Go inside the docker container `app`
```
$ docker exec -it app bash
```
6. Seed database.
```
$ docker exec -it app bash
$ composer update
$ npm install
$ php artisan migrate --seed
```
7. For automatic build of JS and CSS files. From host machine (not inside container), run the following command and keep it open.
```
$ npm run watch
```
8. That's it! Navigate to [http://localhost](http://localhost) to access the application.


**Useful commands:** 
- Stopping all containers
```
$ docker-compose stop
```
- Starting all containers
```
$ docker-compose up -d
```

