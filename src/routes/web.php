<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/admin', 'AdminUIController@index');
$router->get('/phones', 'AdminUIController@phones');  // Gets Phones List to display in the table
$router->post('/schedule', 'AdminUIController@schedule');

$router->get('/subscribe', 'SubscribeController@index');
$router->post('/push/init', 'PushController@init');
$router->post('/push/topline', 'PushController@topline');
$router->post('/push/display', 'PushController@display');
$router->post('/push/audio', 'PushController@audio');


// FOR TESTING ONLY - Simulating the phone receiving the Push message thru http://<phoneIP>/forms/push
$router->post('/forms/push', 'PushController@simulatePhone');


