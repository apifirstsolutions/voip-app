<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Models\Task;
use App\Http\Controllers\PushController;
use Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Check schedules tables for scheduled messages that are not yet sent  
        // that is before or equal to current time

        $schedule->call(function () {            
            $tasks = Task::where('runat', '<=', date('Y-m-d H:m:s'))
                ->whereNull('status')
                ->get();

            foreach ($tasks as $task) {
                $endpoints = explode(',', $task->endpoints);
                
                $pushtype = $task->pushtype;
                $message = $task->message;

                $pushCtrl = new PushController();
                $pushCtrl->_init($endpoints, $pushtype, $message);

                $task->status = 'done';
                $task->save();

                Log::notice('[SCHEDULED TASK] ID::'.$task.' '.$task->endpoints.', '.$pushtype.', '.$message);
            };
        })->everyFiveMinutes();
    }
}
