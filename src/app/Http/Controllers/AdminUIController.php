<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Phone;
use App\Models\Task;
use Log;

class AdminUIController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $phones = Phone::all(); 

        $phonesArr = [];

        foreach ($phones as $phone) {
            array_push($phonesArr, $phone->ip);
        };

        return view('admin', ['endpoints' => $phonesArr]);
    }

    public function phones(Request $request)
    {
        return Phone::all(); 
    }

    public function schedule(Request $request)
    {
        $runat = $request->input('runat');
        $endpoints = $request->input('endpoints');
        $pushtype = $request->input('pushtype');
        $message = $request->input('message');

        $task = new Task();
        $endpointsString = implode(',', $endpoints);

        $utcDate = date('Y-m-d H:m:s', strtotime($runat));

        $task->runat = $utcDate;
        $task->endpoints = $endpointsString;
        $task->pushtype = $pushtype;
        $task->message = $message;
        $task->save();

        Log::notice('[SCHEDULED TASK SET] '.$endpointsString.', '.$pushtype.', '.$message. '||'. date('Y-m-d H:m:s'));
    
        $tasks = Task::where('runat', '<=', date('Y-m-d H:m:s'))
                ->whereNull('status')
                ->get();

        return $tasks;
    }

}
