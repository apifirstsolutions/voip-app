<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Phone;
use Log;

class SubscribeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $setId = $request->query('SetID');
        $ip = $request->query('ip');
        $mac = $request->query('MAC');
        $extn = $request->query('Extn');

        $phone = Phone::where(['set_id' => $setId])->first();

        if(!$phone) {
            $phone = new Phone();
            $phone->set_id = $setId;
        }

        $phone->ip = $ip;
        $phone->mac = $mac;
        $phone->ext = $extn;
        $phone->save();

        Log::notice('[SUBSCRIBED] '.$setId.', '.$ip.', '.$mac.','.$extn);
    }

}
