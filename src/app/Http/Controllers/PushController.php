<?php

namespace App\Http\Controllers;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException; 

use Illuminate\Http\Request;
use App\Models\Phone;
use Log;


class PushController extends Controller
{
    // POST /push/init
    public function init(Request $request)
    {
        $endpoints = $request->input('endpoints');
        $pushtype = $request->input('pushtype');
        $message = $request->input('message');

        $this->_init($endpoints, $pushtype, $message);
    }

    public function _init($endpoints, $pushtype, $message) {
        $href = env('APP_URL').'/push/'.$pushtype;

        $XPost = 
            '<?XML version="1.0"?>
                <Push alert="1" type="'.$pushtype.'" mode="normal">
                    <go href="'.$href.'" method="post">
                        <postfield name="message" value="'.$message.'"/>
                    </go>
                </Push>';

        $XPost = urlencode($XPost);
        $postedfields = "XMLData=$XPost";

        foreach ($endpoints as $endpoint) {
            $this->phonePushInit($endpoint, $postedfields);
            Log::notice('[PUSH INIT] '.$endpoint.', '.$pushtype.', '.$message);

            if ($pushtype === 'audio') {
                // start audio stream
                $audioFile = env('AUDIO_FILES_LOCATION').'/'.$message;

                Log::debug('[DEBUG] '.$endpoint.', '.$pushtype.', '.$message);
                $process = new Process([
                    env('AUDIO_STREAM_SCRIPT'),
                    $audioFile,
                    $endpoint,
                    env('AUDIO_STREAM_PORT')
                ]);
                Log::notice('[AUDIO STREAM START] '.$endpoint.'. Audio file:'.$audioFile);
                $process->run();
            }
        }
    }


    // Initialize PUSH with Phone
    private function phonePushInit($phoneIP, $postedfields) {
        $ch = curl_init();
        $URL = "http://" .$phoneIP . "/forms/push";

        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // store the return value as a string
        curl_setopt($ch, CURLOPT_TIMEOUT, 4); // times out after 4s
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postedfields);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    // POST /push/topline
    public function topline(Request $request)
    {
        $message = $request->input('message');
        $XMLString = 
            '<?XML version="1.0" encoding="UTF-8"?>
            <Response>
                <Topline>'.$message.'</Topline>
            </Response>';

        Log::notice('[PUSH TOPLINE MESSAGE] '.$request->ip().', '.$message);
        return response()->xml($XMLString);
    }

    // POST /push/display
    public function display(Request $request)
    {
        $message = $request->input('message');
        $XMLString = 
            '<?XML version="1.0" encoding="UTF-8"?>
            <wml>
                <card id="Display" title="Display Message">
                    <p>'.$message.'</p>
                </card>    
            </wml>';

        Log::notice('[PUSH DISPLAY MESSAGE] '.$request->ip().', '.$XMLString);
        return response()->xml($XMLString);
    }

    // POST /push/audio
    public function audio(Request $request)
    {
        $message = $request->input('message');
        $XMLString = 
            '<?XML version="1.0"?>
            <Response>
                <Audio codec ="PCMU" packetsize="60">
                <AudioTimer value="12"/>
                    <Url href="RTPRx://'.env('AUDIO_STREAM_SERVER').':'.env('AUDIO_STREAM_PORT').'"/>
                </Audio>
            </Response>';

        Log::notice('[PUSH AUDIO MESSAGE] '.$request->ip().', '.$message);
        return response()->xml($XMLString);
    }

    // FOR TESTING ONLY - Simulate the phone receiving the Push initialization in http://<phoneIP>/forms/push
    public function simulatePhone(Request $request) {
        $XMLData = $request->input('XMLData');
        Log::notice('[DEBUG][PHONE SIMULATION] '.$XMLData);
        return response()->xml($XMLData);
    }
}