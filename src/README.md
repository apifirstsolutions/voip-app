# Emergency-App
## **Production Setup**
### Deploy Package
- Obtain `package.tar.gz` and extract to `/var/www/html/Emergency` 
- Point webserver's public web directory to `/var/www/html/Emergency/src/public`
### Set Environment Variables for Emergency-App
- Copy `Emergency/src/.env.example` to new file `Emergency/src/.env`
- Set the following required environment variables

Variable | Required | Description | Example
--- | --- | --- | ---
APP_NAME | Yes | Application Name | Emergency
APP_ENV | Yes | local or production | local
APP_URL | Yes | Home URL. Also used as Push Server URL | http://127.0.0.1
APP_TIMEZONE | Yes | TZ used by app | UTC
DB_CONNECTION | Yes | DB Driver used. Use `mysql` for mariadb | mysql
DB_HOST | Yes | Database Host | 127.0.0.1
DB_PORT | Yes | Database Port | 3306
DB_DATABASE | Yes | Database Name | appdb
DB_USERNAME | Yes | DB User | user
DB_PASSWORD | Yes | DB User password | userpass
AUDIO_STREAM_SCRIPT | Yes | Location of `stream.sh` | /var/www/html/Emergency/stream.sh
AUDIO_STREAM_PORT | Yes | Audion Stream RTP port | 5000
AUDIO_FILES_LOCATION | Yes | Directory of audio files | /var/www/html/Emergency/src/public/audio


### Run database migration scripts

```
$ /var/www/html/Emergency/artisan migrate
```
### Setup cron to run Lumen scheduler every minute
- Add this in cron
```
$ crontab -e

* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1
```
- Restart cron
```
$ /etc/init.d/cron reload
```
### Setup GStream for RTP audio stream 
- Install GStream if not yet installed
```
$ sudo apt-get install gstreamer1.0-tools
```
- Check is audio streamer workin
```
$ gst-launch-1.0 -m filesrc location=/var/www/html/Emergency/src/public/audio/audio1.wav ! wavparse ! audioconvert ! audioresample ! alawenc ! rtppcmapay ! udpsink host=127.0.0.1 port=5000
```


That's it! Access application at `<APP_URL>/admin`