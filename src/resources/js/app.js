import Vue from 'vue';
import Notifications from 'vue-notification';

Vue.use(Notifications);

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

const files = require.context('./', true, /\.vue$/i)

console.log(files);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

new Vue({
  el: '#app'
});
