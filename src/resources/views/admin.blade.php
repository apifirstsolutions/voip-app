@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-sm-12 col-md-10 col-md-offset-2">
      <accordion  v-bind:endpoints ="{{ json_encode($endpoints) }}"></accordion>
    </div>

    <notifications group="appNotifications" position="top center"></notifications>
     
  
</div>
@endsection
