<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Emergency</title>
    <link rel="stylesheet" href="mini.css" />
    <link rel="stylesheet" href="css/app.css" />
</head>
<body>
    <header style="background-image: url('img/header.png'); height: 150px">
      <div class="container">
        <div class="row"> 
          <div class="col-sm-12 col-md-2 ">
            <a href="#" class="logo" style="color: #ebebeb;">Emergency</a>
          </div>
          <div class="col-sm-12 col-md-3 col-md-offset-6">
            <a href="/admin" class="link">Main</a>
          </div>
        </div>

      </div>
      
    </header>
    <div id="app">
      <main>
        @yield('content')
      </main>
    
    </div>
    <script src="js/app.js"></script>
</body>
</html>